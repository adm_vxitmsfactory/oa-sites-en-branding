﻿
###################
#
# File:        BrandingAcceptatie
# Author:      Ronald Huijbrechtse
# Date:        03-04-2015
# Description: Updating specific css-files, based on a search string ($searchstring), with additional css-content ($css_addition)
# Functions:   UpdateCssFile - Adds additional content to the original css files
#              ResetCssFile - Reset the css file to the original state
# Parameter:   No parameter - Activates the function UpdateCssFile
#              Reset - Activates the function ResetCssFile
#
###################
Param
(
   [switch]$Reset
)
#Global variables
# additional css content
$css_addition = @"
/* Branding specific voor test environment */
body:before {   
  background: red;
  color: white;   
  width: 100%; 
  display: inline-block;   
  clear: both;   
  font-size: 20px;   
  text-align: center;     
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
  content: "T e s t o m g e v i n g";}
"@
# CSS path
$css_path = "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\*\TEMPLATE\LAYOUTS\*\STYLES"
#$css_path = "E:\Working Folder\Opdrachten\Oasen\UpdateCssFiles\*\TEMPLATE\LAYOUTS\*\STYLES"
#$css_path = "E:\Working Folder\Prive\HBO Webdesign\html\HPH"
# String to be search for in the css file
$searchstring = "\bbody\b\s*?\{"


Function ResetCssFile()
{
    Write-Host "Resetting Css files to original"
    $cssfiles = Get-ChildItem $css_path -Include "*.css-original" -Recurse 

        ForEach ($cssfile_org in $cssfiles)
        {
            Write-Host "Processing: " $cssfile_org.FullName -ForegroundColor Green
            $cssfile = $cssfile_org.FullName -replace "-original"
            Write-Host $cssfile
            Remove-Item $cssfile
            Rename-Item $cssfile_org.FullName  $cssfile
        }
}

Function UpdateCssFile()
{ 
	try
	{
		Write-Host "Start updating CSS files ..." -ForegroundColor Yellow

        # Find all css files within the path described in $css_path
        $cssfiles = Get-ChildItem $css_path -Include "*.css" -Recurse 

        ForEach ($cssfile in $cssfiles)
        {
            # 
            [String]$css_content = Get-Content $cssfile.FullName
            If (($css_content | Select-String -Pattern $searchstring) -and !($css_content | Select-String -SimpleMatch "Branding specific voor test environment"))
            {
                Write-Host "Processing: " $cssfile.FullName -ForegroundColor Green

                # Make a backup from the css-file            
                $backupfile = $cssfile.FullName +"-original"
                Write-Host "Creating the backup" $backupfile
                Copy-Item $cssfile.FullName $backupfile -ErrorAction Stop

                #Putting the additional text into the css-file
                Write-Host "Updating the css"
                add-content -path $cssfile.FullName  -value $css_addition
            }
        }
			
		
		Write-Host "Finished updating CSS files ..." -ForegroundColor Yellow
		
	}
	catch 
    {
		Write-Host "Something went wrong " $_.Exception.ToString() -ForegroundColor Red

		Write-Host $_.ScriptStackTrace.ToString() -ForegroundColor Yellow
	}
	
}

If ($Reset)
{
        Write-Host "This will reset all css files for the test environment"
        $confirmation = Read-Host "Are you Sure You Want To Proceed (y/n): "
        if ($confirmation -eq 'y') 
        {
            ResetCssFile
        }
}
Else
{
        Write-Host "This will update all css files for the test environment"
        $confirmation = Read-Host "Are you Sure You Want To Proceed (y/n): "
        if ($confirmation -eq 'y') 
        {
            UpdateCssFile
        }   
}
